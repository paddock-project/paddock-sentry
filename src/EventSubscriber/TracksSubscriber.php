<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Paddock\Apps\Sentry\EventSubscriber;

use BadPixxel\Paddock\Apps\Sentry\Tracks;
use BadPixxel\Paddock\Core\Models\Tracks\AbstractTracksSubscriber;

class TracksSubscriber extends AbstractTracksSubscriber
{
    /**
     * {@inheritDoc}
     */
    public function getStaticTracks(): array
    {
        return array(
            Tracks\SentryHealthChecker::class => array('all' => true),
        );
    }
}
