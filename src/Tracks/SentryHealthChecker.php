<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Paddock\Apps\Sentry\Tracks;

use BadPixxel\Paddock\Apps\Sentry\Collector\SentryStatsCollector;
use BadPixxel\Paddock\Core\Loader\EnvLoader;
use BadPixxel\Paddock\Core\Models\Tracks\AbstractTrack;

class SentryHealthChecker extends AbstractTrack
{
    /**
     * Track Constructor
     */
    public function __construct()
    {
        parent::__construct("sentry-health-checker");
        //====================================================================//
        // Track Configuration
        $this->enabled = !empty(EnvLoader::get("SENTRY_API_URL"));
        $this->description = "[SENTRY] Check Project Health";
        $this->collector = SentryStatsCollector::getCode();

        //====================================================================//
        // Add Rules
        //====================================================================//

        $this->addRule("accepted", array(
            "lte" => array("error" => 100, "warning" => 50 ),
            "metric" => "sentry_accepted"
        ));
        $this->addRule("filtered", array(
            "lte" => array("error" => 500, "warning" => 100 ),
            "metric" => "sentry_filtered"
        ));
        $this->addRule("rate_limited", array(
            "lte" => array("error" => 100, "warning" => 50 ),
            "metric" => "sentry_filtered"
        ));
        $this->addRule("invalid", array(
            "lte" => array("error" => 100, "warning" => 50 ),
            "metric" => "sentry_invalid"
        ));
        $this->addRule("received", array(
            "lte" => array("error" => 500, "warning" => 100),
            "metric" => "sentry_received"
        ));
    }
}
