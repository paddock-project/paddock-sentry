<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

return array(
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => array('all' => true),

    BadPixxel\Paddock\Core\PaddockCoreBundle::class => array('all' => true),
    Knp\Bundle\GaufretteBundle\KnpGaufretteBundle::class => array('all' => true),

    BadPixxel\Paddock\Apps\Sentry\PaddockSentryBundle::class => array('all' => true),
);
